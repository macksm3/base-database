from . import db
from flask_login import UserMixin
from sqlalchemy.sql import func


class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.String(12))
    name = db.Column(db.String(255))
    link = db.Column(db.String(255))
    create_date = db.Column(db.String(12))
    create_by = db.Column(db.String(120))
    lms_number = db.Column(db.String(25))


class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String(10000))
    date = db.Column(db.DateTime(timezone=True), default=func.now())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), unique=True)
    password = db.Column(db.String(150))
    userName = db.Column(db.String(150))
    notes = db.relationship('Note')


